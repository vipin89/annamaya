<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<?php
  $orderId=$_REQUEST['ID'];
  global $wpdb; 
	$ws_table_name = $wpdb->prefix.'orders' ;
	$order_item_table=$wpdb->prefix.'order_items' ;    
	$kv_query = "SELECT * FROM `$ws_table_name` AS ORDERS,`$order_item_table` AS ORDERITEMS WHERE ORDERS.id=$orderId AND ORDERS.id = ORDERITEMS.order_id  ";
	$data=$wpdb->get_results($kv_query);
	
 ?>

<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-10 col-md-offset-1">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Product</th>
                        <th>Quantity</th>
                        <th class="text-center">Price</th>
                        <th class="text-center">Total</th>
                        <th> </th>
                    </tr>
                </thead>
                <tbody>
                    <?php  
					 $total_price = 0;
	                 $total_item = 0;
					foreach($data as $key=>$val){  
                      $productDetails=get_post($val->product_id); 
                      $total_price = $total_price + ($val->quantity * $val->price);  
					?>
                    <tr>
                        <td class="col-md-6">
                        <div class="media">
						      <?php  $image = get_field('product_image',$val->product_id);
							  if (!empty($image)){ ?>
							    <a class="thumbnail pull-left" href="#"> <img class="media-object" src="<?php echo $image; ?>" style="width: 72px; height: 72px;"> </a>
							  <?php } else {  ?>
                            <a class="thumbnail pull-left" href="#"> <img class="media-object" src="http://icons.iconarchive.com/icons/custom-icon-design/flatastic-2/72/product-icon.png" style="width: 72px; height: 72px;"> </a>
							  <?php } ?>
                            <div class="media-body">
                                <h4 class="media-heading"><a href="#"><?php echo $productDetails->post_title ?></a></h4>
                            </div>
                        </div></td>
                         <td class="col-md-1 text-center"><strong><?php echo $val->quantity ; ?></strong></td>
                        <td class="col-md-1 text-center"><strong><?php echo '₹ '.$val->price; ?></strong></td>
                        <td class="col-md-1 text-center"><strong><?php echo '₹ '.$val->quantity * $val->price; ?></strong></td>
                        <td class="col-md-1">
                        </td>
                    </tr>
					<?php } ?>
                    
                    <tr>
                        <td>   </td>
                        <td>   </td>
                        <td>   </td>
                        <td><h3>Total</h3></td>
                        <td class="text-right"><h3><strong><?php echo '₹ '.$total_price ?></strong></h3></td>
                    </tr>
                    <tr>
                        <td>   </td>
                        <td>   </td>
                        <td>   </td>
                        <td>
                       </td>
                        <td>
                        
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>