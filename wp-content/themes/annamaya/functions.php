<?php
/**
 * annay functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Annay
 * @since 1.0.0
 */

/**
 * Table of Contents:
 * Theme Support
 * Required Files
 * Register Styles
 * Register Scripts
 * Register Menus
 * Custom Logo
 * WP Body Open
 * Register Sidebars
 * Enqueue Block Editor Assets
 * Enqueue Classic Editor Styles
 * Block Editor Settings
 */

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */

/**
 * REQUIRED FILES
 * Include required files.
 */
function twentytwenty_theme_support() {

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	// Custom background color.
	add_theme_support(
		'custom-background',
		array(
			'default-color' => 'f5efe0',
		)
	);

	// Set content-width.
	global $content_width;
	if ( ! isset( $content_width ) ) {
		$content_width = 580;
	}

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// Set post thumbnail size.
	set_post_thumbnail_size( 1200, 9999 );

	// Add custom image size used in Cover Template.
	add_image_size( 'twentytwenty-fullscreen', 1980, 9999 );

	// Custom logo.
	$logo_width  = 120;
	$logo_height = 90;

	// If the retina setting is active, double the recommended width and height.
	if ( get_theme_mod( 'retina_logo', false ) ) {
		$logo_width  = floor( $logo_width * 2 );
		$logo_height = floor( $logo_height * 2 );
	}

	add_theme_support(
		'custom-logo',
		array(
			'height'      => $logo_height,
			'width'       => $logo_width,
			'flex-height' => true,
			'flex-width'  => true,
		)
	);

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support(
		'html5',
		array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'script',
			'style',
		)
	);

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Twenty Twenty, use a find and replace
	 * to change 'twentytwenty' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'twentytwenty' );

	// Add support for full and wide align images.
	add_theme_support( 'align-wide' );

	/*
	 * Adds starter content to highlight the theme on fresh sites.
	 * This is done conditionally to avoid loading the starter content on every
	 * page load, as it is a one-off operation only needed once in the customizer.
	 */
	if ( is_customize_preview() ) {
		require get_template_directory() . '/inc/starter-content.php';
		add_theme_support( 'starter-content', twentytwenty_get_starter_content() );
	}

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/*
	 * Adds `async` and `defer` support for scripts registered or enqueued
	 * by the theme.
	 */
	$loader = new TwentyTwenty_Script_Loader();
	add_filter( 'script_loader_tag', array( $loader, 'filter_script_loader_tag' ), 10, 2 );

}

add_action( 'after_setup_theme', 'twentytwenty_theme_support' );

require get_template_directory() . '/inc/template-tags.php';

// Handle SVG icons.
require get_template_directory() . '/classes/class-twentytwenty-svg-icons.php';
require get_template_directory() . '/inc/svg-icons.php';


// Handle Customizer settings.
require get_template_directory() . '/classes/class-twentytwenty-customize.php';

// Require Separator Control class.
require get_template_directory() . '/classes/class-twentytwenty-separator-control.php';

// Custom comment walker.
require get_template_directory() . '/classes/class-twentytwenty-walker-comment.php';

// Custom page walker.
require get_template_directory() . '/classes/class-twentytwenty-walker-page.php';

// Custom script loader class.
require get_template_directory() . '/classes/class-twentytwenty-script-loader.php';
// Non-latin language handling.
require get_template_directory() . '/classes/class-twentytwenty-non-latin-languages.php';


// Custom page walker.
require get_template_directory() . '/classes/class-wp-bootstrap-navwalker.php';
/**
 * Register and Enqueue Styles.
 */
function annay_register_styles() {

	$theme_version = wp_get_theme()->get( 'Version' );

	//wp_enqueue_style( 'annay-style', get_stylesheet_uri(), array(), $theme_version );
	//wp_style_add_data( 'annay-style', 'rtl', 'replace' );  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
	// Load all of the styles that need to appear on all pages  
	wp_enqueue_style( 'font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css', array(), $theme_version  );
	wp_enqueue_style( 'bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css', array(), $theme_version  );
	wp_enqueue_style( 'playfair', get_template_directory_uri() . '/assets/fonts/playfair/stylesheet.css', array(), $theme_version  );
	wp_enqueue_style( 'montserrat', get_template_directory_uri() . '/assets/fonts/montserrat/stylesheet.css', array(), $theme_version  );
	wp_enqueue_style( 'opensans', get_template_directory_uri() . '/assets/fonts/opensans/stylesheet.css' , array(), $theme_version );
	wp_enqueue_style( 'linearicons', get_template_directory_uri() . '/assets/fonts/linearicons/stylesheet.css', array(), $theme_version  );
	wp_enqueue_style( 'ionicons', get_template_directory_uri() . '/assets/fonts/ionicons.min.css', array(), $theme_version  );
	wp_enqueue_style( 'pageloader', get_template_directory_uri() . '/assets/css/pageloader.css', array(), $theme_version  );
	wp_enqueue_style( 'foundation', get_template_directory_uri() . '/assets/css/foundation.min.css', array(), $theme_version  );
	wp_enqueue_style( 'swiper', get_template_directory_uri() . '/assets/js/vendor/swiper.min.css', array(), $theme_version  );
	wp_enqueue_style( 'fullpage', get_template_directory_uri() . '/assets/js/vendor/jquery.fullpage.min.css', array(), $theme_version  );
	wp_enqueue_style( 'vegas', get_template_directory_uri() . '/assets/js/vegas/vegas.min.css', array(), $theme_version  );
	wp_enqueue_style( 'owl-carousel', get_template_directory_uri() . '/assets/js/owl-carousel/owl.carousel.css', array(), $theme_version  );
	wp_enqueue_style( 'main', get_template_directory_uri() . '/assets/css/main.css' , array(), $theme_version );
	wp_enqueue_style( 'style-color', get_template_directory_uri() . '/assets/css/style-color.css' , array(), $theme_version );
	wp_enqueue_style( 'style-color3', get_template_directory_uri() . '/assets/css/style-color3.css', array(), $theme_version  );
	
	wp_enqueue_style( 'animate', get_template_directory_uri() . '/assets/vendor/animate/animate.css', array(), $theme_version  );
	wp_enqueue_style( 'hamburgers', get_template_directory_uri() . '/assets/vendor/css-hamburgers/hamburgers.min.css' , array(), $theme_version );
	wp_enqueue_style( 'animsition', get_template_directory_uri() . '/assets/vendor/animsition/css/animsition.min.css' , array(), $theme_version );
	wp_enqueue_style( 'select2', get_template_directory_uri() . '/assets/vendor/select2/select2.min.css', array(), $theme_version  );
	




}

add_action( 'wp_enqueue_scripts', 'annay_register_styles' );

/**
 * Register and Enqueue Scripts.
 */
function annay_register_scripts() {

	$theme_version = wp_get_theme()->get( 'Version' );

	//wp_enqueue_script( 'annay-js', get_template_directory_uri() . '/assets/js/index.js', array(), $theme_version, false );
	//wp_script_add_data( 'annay-js', 'async', true );
	 wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/js/vendor/modernizr-2.7.1.min.js' , array(), $theme_version,  false );
	 /* wp_enqueue_script( 'jquery', get_template_directory_uri() . '/assets/js/vendor/jquery-1.12.4.min.js' , array(), $theme_version,  false );
	 wp_enqueue_script( 'all', get_template_directory_uri() . '/assets/js/vendor/all.js' , array(), $theme_version,  false );
	 wp_enqueue_script( 'particles', get_template_directory_uri() . '/assets/js/particlejs/particles.min.js' , array(), $theme_version,  false );
	 wp_enqueue_script( 'downCount', get_template_directory_uri() . '/assets/js/jquery.downCount.js' , array(), $theme_version,  false );
	 wp_enqueue_script( 'form_script', get_template_directory_uri() . '/assets/js/form_script.js' , array(), $theme_version,  false );
	 wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/js/main.js' , array(), $theme_version,  false ); */

}

add_action( 'wp_enqueue_scripts', 'annay_register_scripts' );
add_filter('show_admin_bar', '__return_false');

/**
 * Register navigation menus uses wp_nav_menu in five places.
 */
function annay_menus() {

	$locations = array(
		'primary'  => __( 'Desktop Horizontal Menu', 'annay' ),
		'expanded' => __( 'Desktop Expanded Menu', 'annay' ),
		'mobile'   => __( 'Mobile Menu', 'annay' ),
		'footer'   => __( 'Footer Menu', 'annay' ),
		'social'   => __( 'Social Menu', 'annay' ),
	);

	register_nav_menus( $locations );
}

add_action( 'init', 'annay_menus' );

/**
 * Get accessible color for an area.
 *
 * @since 1.0.0
 *
 * @param string $area The area we want to get the colors for.
 * @param string $context Can be 'text' or 'accent'.
 * @return string Returns a HEX color.
 */
function twentytwenty_get_color_for_area( $area = 'content', $context = 'text' ) {

	// Get the value from the theme-mod.
	$settings = get_theme_mod(
		'accent_accessible_colors',
		array(
			'content'       => array(
				'text'      => '#000000',
				'accent'    => '#cd2653',
				'secondary' => '#6d6d6d',
				'borders'   => '#dcd7ca',
			),
			'header-footer' => array(
				'text'      => '#000000',
				'accent'    => '#cd2653',
				'secondary' => '#6d6d6d',
				'borders'   => '#dcd7ca',
			),
		)
	);

	// If we have a value return it.
	if ( isset( $settings[ $area ] ) && isset( $settings[ $area ][ $context ] ) ) {
		return $settings[ $area ][ $context ];
	}

	// Return false if the option doesn't exist.
	return false;
}

/**
 * Returns an array of variables for the customizer preview.
 *
 * @since 1.0.0
 *
 * @return array
 */
function twentytwenty_get_customizer_color_vars() {
	$colors = array(
		'content'       => array(
			'setting' => 'background_color',
		),
		'header-footer' => array(
			'setting' => 'header_footer_background_color',
		),
	);
	return $colors;
}

/**
 * Get an array of elements.
 *
 * @since 1.0
 *
 * @return array
 */
function twentytwenty_get_elements_array() {

	// The array is formatted like this:
	// [key-in-saved-setting][sub-key-in-setting][css-property] = [elements].
	$elements = array(
		'content'       => array(
			'accent'     => array(
				'color'            => array( '.color-accent', '.color-accent-hover:hover', '.color-accent-hover:focus', ':root .has-accent-color', '.has-drop-cap:not(:focus):first-letter', '.wp-block-button.is-style-outline', 'a' ),
				'border-color'     => array( 'blockquote', '.border-color-accent', '.border-color-accent-hover:hover', '.border-color-accent-hover:focus' ),
				'background-color' => array( 'button:not(.toggle)', '.button', '.faux-button', '.wp-block-button__link', '.wp-block-file .wp-block-file__button', 'input[type="button"]', 'input[type="reset"]', 'input[type="submit"]', '.bg-accent', '.bg-accent-hover:hover', '.bg-accent-hover:focus', ':root .has-accent-background-color', '.comment-reply-link' ),
				'fill'             => array( '.fill-children-accent', '.fill-children-accent *' ),
			),
			'background' => array(
				'color'            => array( ':root .has-background-color', 'button', '.button', '.faux-button', '.wp-block-button__link', '.wp-block-file__button', 'input[type="button"]', 'input[type="reset"]', 'input[type="submit"]', '.wp-block-button', '.comment-reply-link', '.has-background.has-primary-background-color:not(.has-text-color)', '.has-background.has-primary-background-color *:not(.has-text-color)', '.has-background.has-accent-background-color:not(.has-text-color)', '.has-background.has-accent-background-color *:not(.has-text-color)' ),
				'background-color' => array( ':root .has-background-background-color' ),
			),
			'text'       => array(
				'color'            => array( 'body', '.entry-title a', ':root .has-primary-color' ),
				'background-color' => array( ':root .has-primary-background-color' ),
			),
			'secondary'  => array(
				'color'            => array( 'cite', 'figcaption', '.wp-caption-text', '.post-meta', '.entry-content .wp-block-archives li', '.entry-content .wp-block-categories li', '.entry-content .wp-block-latest-posts li', '.wp-block-latest-comments__comment-date', '.wp-block-latest-posts__post-date', '.wp-block-embed figcaption', '.wp-block-image figcaption', '.wp-block-pullquote cite', '.comment-metadata', '.comment-respond .comment-notes', '.comment-respond .logged-in-as', '.pagination .dots', '.entry-content hr:not(.has-background)', 'hr.styled-separator', ':root .has-secondary-color' ),
				'background-color' => array( ':root .has-secondary-background-color' ),
			),
			'borders'    => array(
				'border-color'        => array( 'pre', 'fieldset', 'input', 'textarea', 'table', 'table *', 'hr' ),
				'background-color'    => array( 'caption', 'code', 'code', 'kbd', 'samp', '.wp-block-table.is-style-stripes tbody tr:nth-child(odd)', ':root .has-subtle-background-background-color' ),
				'border-bottom-color' => array( '.wp-block-table.is-style-stripes' ),
				'border-top-color'    => array( '.wp-block-latest-posts.is-grid li' ),
				'color'               => array( ':root .has-subtle-background-color' ),
			),
		),
		'header-footer' => array(
			'accent'     => array(
				'color'            => array( 'body:not(.overlay-header) .primary-menu > li > a', 'body:not(.overlay-header) .primary-menu > li > .icon', '.modal-menu a', '.footer-menu a, .footer-widgets a', '#site-footer .wp-block-button.is-style-outline', '.wp-block-pullquote:before', '.singular:not(.overlay-header) .entry-header a', '.archive-header a', '.header-footer-group .color-accent', '.header-footer-group .color-accent-hover:hover' ),
				'background-color' => array( '.social-icons a', '#site-footer button:not(.toggle)', '#site-footer .button', '#site-footer .faux-button', '#site-footer .wp-block-button__link', '#site-footer .wp-block-file__button', '#site-footer input[type="button"]', '#site-footer input[type="reset"]', '#site-footer input[type="submit"]' ),
			),
			'background' => array(
				'color'            => array( '.social-icons a', 'body:not(.overlay-header) .primary-menu ul', '.header-footer-group button', '.header-footer-group .button', '.header-footer-group .faux-button', '.header-footer-group .wp-block-button:not(.is-style-outline) .wp-block-button__link', '.header-footer-group .wp-block-file__button', '.header-footer-group input[type="button"]', '.header-footer-group input[type="reset"]', '.header-footer-group input[type="submit"]' ),
				'background-color' => array( '#site-header', '.footer-nav-widgets-wrapper', '#site-footer', '.menu-modal', '.menu-modal-inner', '.search-modal-inner', '.archive-header', '.singular .entry-header', '.singular .featured-media:before', '.wp-block-pullquote:before' ),
			),
			'text'       => array(
				'color'               => array( '.header-footer-group', 'body:not(.overlay-header) #site-header .toggle', '.menu-modal .toggle' ),
				'background-color'    => array( 'body:not(.overlay-header) .primary-menu ul' ),
				'border-bottom-color' => array( 'body:not(.overlay-header) .primary-menu > li > ul:after' ),
				'border-left-color'   => array( 'body:not(.overlay-header) .primary-menu ul ul:after' ),
			),
			'secondary'  => array(
				'color' => array( '.site-description', 'body:not(.overlay-header) .toggle-inner .toggle-text', '.widget .post-date', '.widget .rss-date', '.widget_archive li', '.widget_categories li', '.widget cite', '.widget_pages li', '.widget_meta li', '.widget_nav_menu li', '.powered-by-wordpress', '.to-the-top', '.singular .entry-header .post-meta', '.singular:not(.overlay-header) .entry-header .post-meta a' ),
			),
			'borders'    => array(
				'border-color'     => array( '.header-footer-group pre', '.header-footer-group fieldset', '.header-footer-group input', '.header-footer-group textarea', '.header-footer-group table', '.header-footer-group table *', '.footer-nav-widgets-wrapper', '#site-footer', '.menu-modal nav *', '.footer-widgets-outer-wrapper', '.footer-top' ),
				'background-color' => array( '.header-footer-group table caption', 'body:not(.overlay-header) .header-inner .toggle-wrapper::before' ),
			),
		),
	);

	/**
	* Filters Twenty Twenty theme elements
	*
	* @since 1.0.0
	*
	* @param array Array of elements
	*/
	return apply_filters( 'twentytwenty_get_elements_array', $elements );
}


/**
 * Fix skip link focus in IE11.
 *
 * This does not enqueue the script because it is tiny and because it is only for IE11,
 * thus it does not warrant having an entire dedicated blocking script being loaded.
 *
 * @link https://git.io/vWdr2
 */
function twentytwenty_skip_link_focus_fix() {
	// The following is minified via `terser --compress --mangle -- assets/js/skip-link-focus-fix.js`.
	?>
	<script>
	/(trident|msie)/i.test(navigator.userAgent)&&document.getElementById&&window.addEventListener&&window.addEventListener("hashchange",function(){var t,e=location.hash.substring(1);/^[A-z0-9_-]+$/.test(e)&&(t=document.getElementById(e))&&(/^(?:a|select|input|button|textarea)$/i.test(t.tagName)||(t.tabIndex=-1),t.focus())},!1);
	</script>
	<?php
}
add_action( 'wp_print_footer_scripts', 'twentytwenty_skip_link_focus_fix' );

/** Enqueue non-latin language styles
 *
 * @since 1.0.0
 *
 * @return void
 */
function twentytwenty_non_latin_languages() {
	$custom_css = TwentyTwenty_Non_Latin_Languages::get_non_latin_css( 'front-end' );

	if ( $custom_css ) {
		wp_add_inline_style( 'twentytwenty-style', $custom_css );
	}
}

add_action( 'wp_enqueue_scripts', 'twentytwenty_non_latin_languages' );

/**
 * Get the information about the logo.
 *
 * @param string $html The HTML output from get_custom_logo (core function).
 *
 * @return string $html
 */
function twentytwenty_get_custom_logo( $html ) {

	$logo_id = get_theme_mod( 'custom_logo' );

	if ( ! $logo_id ) {
		return $html;
	}

	$logo = wp_get_attachment_image_src( $logo_id, 'full' );

	if ( $logo ) {
		// For clarity.
		$logo_width  = esc_attr( $logo[1] );
		$logo_height = esc_attr( $logo[2] );

		// If the retina logo setting is active, reduce the width/height by half.
		if ( get_theme_mod( 'retina_logo', false ) ) {
			$logo_width  = floor( $logo_width / 2 );
			$logo_height = floor( $logo_height / 2 );

			$search = array(
				'/width=\"\d+\"/iU',
				'/height=\"\d+\"/iU',
			);

			$replace = array(
				"width=\"{$logo_width}\"",
				"height=\"{$logo_height}\"",
			);

			// Add a style attribute with the height, or append the height to the style attribute if the style attribute already exists.
			if ( strpos( $html, ' style=' ) === false ) {
				$search[]  = '/(src=)/';
				$replace[] = "style=\"height: {$logo_height}px;\" src=";
			} else {
				$search[]  = '/(style="[^"]*)/';
				$replace[] = "$1 height: {$logo_height}px;";
			}

			$html = preg_replace( $search, $replace, $html );

		}
	}

	return $html;

}

add_filter( 'get_custom_logo', 'twentytwenty_get_custom_logo' );

if ( ! function_exists( 'wp_body_open' ) ) {

	/**
	 * Shim for wp_body_open, ensuring backwards compatibility with versions of WordPress older than 5.2.
	 */
	function wp_body_open() {
		do_action( 'wp_body_open' );
	}
}

/**
 * Include a skip to content link at the top of the page so that users can bypass the menu.
 */
function twentytwenty_skip_link() {
	echo '<a class="skip-link screen-reader-text" href="#site-content">' . __( 'Skip to the content', 'twentytwenty' ) . '</a>';
}

add_action( 'wp_body_open', 'twentytwenty_skip_link', 5 );

/**
 * Register widget areas.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function twentytwenty_sidebar_registration() {

	// Arguments used in all register_sidebar() calls.
	$shared_args = array(
		'before_title'  => '<h2 class="widget-title subheading heading-size-3">',
		'after_title'   => '</h2>',
		'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
		'after_widget'  => '</div></div>',
	);

	// Footer #1.
	register_sidebar(
		array_merge(
			$shared_args,
			array(
				'name'        => __( 'Footer #1', 'twentytwenty' ),
				'id'          => 'sidebar-1',
				'description' => __( 'Widgets in this area will be displayed in the first column in the footer.', 'twentytwenty' ),
			)
		)
	);

	// Footer #2.
	register_sidebar(
		array_merge(
			$shared_args,
			array(
				'name'        => __( 'Footer #2', 'twentytwenty' ),
				'id'          => 'sidebar-2',
				'description' => __( 'Widgets in this area will be displayed in the second column in the footer.', 'twentytwenty' ),
			)
		)
	);

}

add_action( 'widgets_init', 'twentytwenty_sidebar_registration' );

function add_to_cart() {
 session_start();
if(isset($_POST["action"]))
{
 if($_POST["action"] == "add_to_cart")
 {
  if(isset($_SESSION["shopping_cart"]))
  {
   $is_available = 0;
   foreach($_SESSION["shopping_cart"] as $keys => $values)
   {
    if($_SESSION["shopping_cart"][$keys]['product_id'] == $_POST["product_id"])
    {
     $is_available++;
     $_SESSION["shopping_cart"][$keys]['product_quantity'] = $_SESSION["shopping_cart"][$keys]['product_quantity'] + $_POST["product_quantity"];
    }
   }
   if($is_available == 0)
   {
    $item_array = array(
     'product_id'               =>     $_POST["product_id"],  
     'product_name'             =>     $_POST["product_name"],  
     'product_price'            =>     $_POST["product_price"],  
     'product_quantity'         =>     $_POST["product_quantity"]
    );
    $_SESSION["shopping_cart"][] = $item_array;
   }
  }
  else
  {
	 
   $item_array[] = array(
    'product_id'               =>     $_POST["product_id"],  
    'product_name'             =>     $_POST["product_name"],  
    'product_price'            =>     $_POST["product_price"],  
    'product_quantity'         =>     $_POST["product_quantity"]
   );
   $_SESSION["shopping_cart"][] = $item_array;
   
  }
  }
 }
}

add_action( 'wp_ajax_nopriv_add_to_cart', 'add_to_cart' );
add_action( 'wp_ajax_add_to_cart', 'add_to_cart' );

function fetch_cart() {
session_start();

$total_price = 0;
$total_item = 0;

$output = '
<div class="table-responsive" id="order_table">
 <table class="table table-bordered table-striped">
  <tr>  
            <th width="40%">Product Name</th>  
            <th width="10%">Quantity</th>  
            <th width="20%">Price</th>  
            <th width="15%">Total</th>  
            <th width="5%">Action</th>  
        </tr>
';
if(!empty($_SESSION["shopping_cart"]))
{
 foreach($_SESSION["shopping_cart"] as $keys => $values)
 {
  $output .= '
  <tr>
   <td>'.$values["product_name"].'</td>
   <td>'.$values["product_quantity"].'</td>
   <td align="right">₹ '.$values["product_price"].'</td>
   <td align="right">₹ '.number_format($values["product_quantity"] * $values["product_price"], 2).'</td>
   <td><button name="delete" class="btn btn-danger btn-xs delete" id="'. $values["product_id"].'">Remove</button></td>
  </tr>
  ';
  $total_price = $total_price + ($values["product_quantity"] * $values["product_price"]);
  $total_item = $total_item + 1;
 }
 $output .= '
 <tr>  
        <td colspan="3" align="right">Total</td>  
        <td align="right">₹ '.number_format($total_price, 2).'</td>  
        <td></td>  
    </tr>
 ';
}
else
{
 $output .= '
    <tr>
     <td colspan="5" align="center">
      Your Cart is Empty!
     </td>
    </tr>
    ';
}
$output .= '</table></div>';
$data = array(
 'cart_details'  => $output,
 'total_price'  => '₹' . number_format($total_price, 2),
 'total_item'  => $total_item
); 

echo json_encode($data); exit;

}

add_action( 'wp_ajax_nopriv_fetch_cart', 'fetch_cart' );
add_action( 'wp_ajax_fetch_cart', 'fetch_cart' );

function remove_cart() {

session_start();

	if(isset($_POST["action"]))
	{

	 if($_POST["action"] == 'remove_cart')
	 {
	  foreach($_SESSION["shopping_cart"] as $keys => $values)
	  {
	   if($values["product_id"] == $_POST["product_id"])
	   {
		unset($_SESSION["shopping_cart"][$keys]);
	   }
	  }
	 }
	}

}

add_action( 'wp_ajax_nopriv_remove_cart', 'remove_cart' );
add_action( 'wp_ajax_remove_cart', 'remove_cart' );

function empty_cart() {
 session_start();

	if(isset($_POST["action"]))
	{
	 if($_POST["action"] == 'empty_cart')
	 {
	  unset($_SESSION["shopping_cart"]);
	 }
	}

}

add_action( 'wp_ajax_nopriv_empty_cart', 'empty_cart' );
add_action( 'wp_ajax_empty_cart', 'empty_cart' );

add_action( 'admin_post_create_order', 'create_order' );
add_action( 'admin_post_nopriv_create_order', 'create_order' );

function create_order() {
	session_start();
	global $wpdb;
	$wp_orders ='wp_orders';
	$wp_order_items ='wp_order_items';
	$total_price = 0;
	$total_item = 0;
	if(!empty($_SESSION["shopping_cart"]))
	{
		$orderInfo=array(
		  'userName'=>$_POST['name'],
		  'userEmail'=>$_POST['contactnumber'],
		  'userContact'=>$_POST['email'],
		  'userMsg'=>$_POST['message'],
		  'status'=> 'pending',
		  'updated_at'=> date('Y-m-d H:i:s'),
		  'created_at'=> date('Y-m-d H:i:s'),
		);
		 $wpdb->insert($wp_orders,$orderInfo);
		 $InsertOrderId = $wpdb->insert_id;
		 foreach($_SESSION["shopping_cart"] as $keys => $values)
		 {
			 $orderItem=array(
			  'order_id'=>$InsertOrderId,
			  'product_id'=>$values["product_id"],
			  'price'=>$values["product_price"],
			  'quantity'=>$values["product_quantity"],
			  'updated_at'=> date('Y-m-d H:i:s'),
			  'created_at'=> date('Y-m-d H:i:s'),
			);
			$wpdb->insert($wp_order_items,$orderItem);
			$total_price = $total_price + ($values["product_quantity"] * $values["product_price"]);
			$total_item = $total_item + 1;
		 }
		 
		 $orderInfoUpdate=array(
		  'total_paid_amount'=>$total_price,
		  'status'=> 'processing',
		  'updated_at'=> time(),
		);
		 $wpdb->update($wp_orders,$orderInfoUpdate,array('id'=>$InsertOrderId));
		  unset($_SESSION["shopping_cart"]);
		 wp_redirect('thank-you');
           exit;
	}
}

/* Custom Post Type Start */
function create_posttype() {
register_post_type( 'news',
// CPT Options
array(
  'labels' => array(
   'name' => __( 'news' ),
   'singular_name' => __( 'News' )
  ),
  'public' => true,
  'has_archive' => false,
  'rewrite' => array('slug' => 'news'),
 )
);
}
// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );

/*Custom Post type start*/

function cw_post_type_news() {

$supports = array(
'title', // post title
'editor', // post content
'author', // post author
'thumbnail', // featured images
'excerpt', // post excerpt
'custom-fields', // custom fields
'comments', // post comments
'revisions', // post revisions
'post-formats', // post formats
);

$labels = array(
'name' => _x('News', 'plural'),
'singular_name' => _x('News', 'singular'),
'menu_name' => _x('News', 'admin menu'),
'name_admin_bar' => _x('News', 'admin bar'),
'add_new' => _x('Add New', 'add new'),
'add_new_item' => __('Add New news'),
'new_item' => __('New news'),
'edit_item' => __('Edit news'),
'view_item' => __('View news'),
'all_items' => __('All news'),
'search_items' => __('Search news'),
'not_found' => __('No news found.'),
);

$args = array(
'supports' => $supports,
'labels' => $labels,
'public' => true,
'query_var' => true,
'rewrite' => array('slug' => 'news'),
'has_archive' => true,
'hierarchical' => false,
);
register_post_type('news', $args);
}
add_action('init', 'cw_post_type_news');

/*Custom Post type end*/


function menu_register() {
	$supports = array(
		'title', // post title
		'editor', // post content
		'author', // post author
		'thumbnail', // featured images
		'excerpt', // post excerpt
		'custom-fields', // custom fields
		'comments', // post comments
		'revisions', // post revisions
		'post-formats', // post formats
		);

    $labels = array(
        'name' => _x('Menu', 'post type general name'),
        'singular_name' => _x('Menu Item', 'post type singular name'),
        'add_new' => _x('Add Menu', 'menu item'),
        'add_new_item' => __('Add New Menu Item'),
        'edit_item' => __('Edit Menu Item'),
        'new_item' => __('New Menu Item'),
        'view_item' => __('View Menu Item'),
        'search_items' => __('Search Menu Items'),
        'not_found' =>  __('Nothing found'),
        'not_found_in_trash' => __('Nothing found in Trash'),
        'parent_item_colon' => ''
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'menu'),
        'capability_type' => 'post',
		'has_archive' => true,
        'hierarchical' => false,
        'menu_position' => 8,
        'supports' => $supports
    ); 
	
    register_post_type( 'menu' , $args );
}
add_action('init', 'menu_register');


function create_menu_taxonomies() {
    $labels = array(
        'name'              => _x( 'Menu Categories', 'taxonomy general name' ),
        'singular_name'     => _x( 'Menu Category', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Menu Categories' ),
        'all_items'         => __( 'All Menu Categories' ),
        'parent_item'       => __( 'Parent Menu Category' ),
        'parent_item_colon' => __( 'Parent Menu Category:' ),
        'edit_item'         => __( 'Edit Menu Category' ),
        'update_item'       => __( 'Update Menu Category' ),
        'add_new_item'      => __( 'Add New Menu Category' ),
        'new_item_name'     => __( 'New Menu Category Name' ),
        'menu_name'         => __( 'Menu Categories' ),
    );

    $args = array(
        'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'menu_categories' ),
    );

    register_taxonomy( 'menu_categories', array( 'menu' ), $args );
}
add_action( 'init', 'create_menu_taxonomies', 0 );

function create_menu_item_taxonomies() {
    $labels = array(
        'name'              => _x( 'Menu Item Categories', 'taxonomy general name' ),
        'singular_name'     => _x( 'Menu Item Category', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Item Menu Categories' ),
        'all_items'         => __( 'All Item Menu Categories' ),
        'parent_item'       => __( 'Parent Item Menu Category' ),
        'parent_item_colon' => __( 'Parent Item Menu Category:' ),
        'edit_item'         => __( 'EditItem Menu Category' ),
        'update_item'       => __( 'Update Item Menu Category' ),
        'add_new_item'      => __( 'Add Item New Menu Category' ),
        'new_item_name'     => __( 'New Item Menu Category Name' ),
        'menu_name'         => __( 'Menu Item Categories' ),
    );

    $args = array(
        'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'menu_item_categories' ),
    );

    register_taxonomy( 'menu_item_categories', array( 'menu' ), $args );
}
add_action( 'init', 'create_menu_item_taxonomies', 1 );

/* Custom Post Type Start */
function create_eatstory() {
register_post_type( 'eatstories',
// CPT Options
array(
  'labels' => array(
   'name' => __( 'Eat Stories' ),
   'singular_name' => __( 'Eat Stories' )
  ),
  'public' => true,
  'has_archive' => false,
  'rewrite' => array('slug' => 'eatstories'),
 )
);
}
// Hooking up our function to theme setup
add_action( 'init', 'create_eatstory' );

/*Custom Post type start*/

function cw_post_type_eat_stories() {

$supports = array(
'title', // post title
'editor', // post content
'author', // post author
'thumbnail', // featured images
'excerpt', // post excerpt
'custom-fields', // custom fields
'comments', // post comments
'revisions', // post revisions
'post-formats', // post formats
);

$labels = array(
'name' => _x('Eat Stories', 'plural'),
'singular_name' => _x('Eat Stories', 'singular'),
'menu_name' => _x('Eat Stories', 'admin menu'),
'name_admin_bar' => _x('Eat Stories', 'admin bar'),
'add_new' => _x('Add Eat Stories', 'add new'),
'add_new_item' => __('Add New Eat Stories'),
'new_item' => __('New eat stories'),
'edit_item' => __('Edit eat stories'),
'view_item' => __('View eat stories'),
'all_items' => __('All eat stories'),
'search_items' => __('Search eat stories'),
'not_found' => __('No eat stories found.'),
);

$args = array(
'supports' => $supports,
'labels' => $labels,
'public' => true,
'query_var' => true,
'rewrite' => array('slug' => 'eatstories'),
'has_archive' => true,
'hierarchical' => false,
);
register_post_type('eatstories', $args);
}
add_action('init', 'cw_post_type_eat_stories');

/*Custom Post type end*/


/*Custom Post type start*/

function cw_post_type_chef() {

$supports = array(
'title', // post title
'editor', // post content
'author', // post author
'thumbnail', // featured images
'excerpt', // post excerpt
'custom-fields', // custom fields
'comments', // post comments
'revisions', // post revisions
'post-formats', // post formats
);

$labels = array(
'name' => _x('Chef', 'plural'),
'singular_name' => _x('Chefs', 'singular'),
'menu_name' => _x('Chef', 'admin menu'),
'name_admin_bar' => _x('Chef', 'admin bar'),
'add_new' => _x('Add Chef', 'add new'),
'add_new_item' => __('Add New Chef'),
'new_item' => __('New Chef'),
'edit_item' => __('Edit Chef'),
'view_item' => __('View Chef'),
'all_items' => __('All Chef'),
'search_items' => __('Search Chef'),
'not_found' => __('No Chef found.'),
);

$args = array(
'supports' => $supports,
'labels' => $labels,
'public' => true,
'query_var' => true,
'rewrite' => array('slug' => 'chef'),
'has_archive' => true,
'hierarchical' => false,
);
register_post_type('chef', $args);
}
add_action('init', 'cw_post_type_chef');

/*Custom Post type end*/










