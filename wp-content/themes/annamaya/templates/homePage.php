<?php
/**
 *
Template Name: Home Page Template
*/
get_header();
?>
<!-- END OF menu button page navigation -->
    <!-- BEGIN OF page cover -->
	<div class="page-cover hh-cover">
		<!-- Cover Background -->
		<div class="cover-bg pos-abs full-size bg-img  bg-blur-0" data-image-src="<?php echo get_template_directory_uri() ?>/assets/images/bg-default1.jpg"></div>

		<!--BEGIN OF Static video bg - uncomment below to use Video as Background-->
		<?php if( get_field('banner_background_video') ): ?>
			<div id="container" class="video-container">
				<video autoplay="autoplay" loop="loop" width="640" height="360">
					<source src="<?php the_field('banner_background_video'); ?>" type="video/mp4">
				</video>
			</div>
		<?php endif; ?>
		<!--END OF Static video bg-->

		<!-- Gradient mask as filter -->
		<!--<div class="cover-bg-mask pos-abs full-size bg-gradient" data-gradient-1="#6c0dff" data-gradient-2="#f9228c" data-gradient-3="#000000" data-opacity="1"></div>-->
		<div class="cover-bg-mask pos-abs full-size bg-gradient" data-gradient-1="#000000" data-gradient-2="rgba(0, 0, 0, 0.2)" data-gradient-3="#000000"
		 data-opacity="0.8"></div>

		<!-- Transluscent mask as filter -->
		<!-- <div class="cover-bg-mask pos-abs full-size bg-color" data-bgcolor="rgba(0, 0, 0, 0.7)"></div> -->

		<div id="main-page-bg" class="main-page-bg cover-bg-mask pos-abs full-size"></div>
	</div>
	<!--END OF page cover -->
	<!-- BEGIN OF page main content -->
	<main class="page-main hh-main page-home fullpg" id="mainpage">

		<!-- Begin of home section -->
		<div class="section section-home" data-section="home">
		
			<!-- Content -->
			<section class="content with-clock anim">
		
				<!-- texts -->
				<div class="c-text">
					<div class="home-wrapper">
						<!-- Title and description -->
						<div class="title-desc">
							<div class="t-wrapper">
								<div class="c-logo home-logo ">
								<?php if( get_field('banner_logo') ): ?>
									<img class="" src="<?php the_field('banner_logo'); ?>" alt="Logo">
								<?php endif; ?>
								</div>
								<header class="header">
								    <?php if( get_field('banner_title') ): ?>
									<h2><?php the_field('banner_title'); ?></h2>
								     <?php endif; ?>
								</header>
								<div class="separator"></div>
								<div class="desc">
								     <?php if( get_field('banner_description') ): ?>
									   <p><?php the_field('banner_description'); ?></p>
								     <?php endif; ?>
								</div>
							</div>
						</div>
		
						<!-- An action button -->
						<div class="home-btns">
						   <?php if( get_field('banner_button_link') ): ?>
									<a class="btn" href="<?php the_field('banner_button_link'); ?>">
										<span class="txt"><?php the_field('banner_button'); ?></span>
										<span class="arrow-icon"></span>
									</a>
							<?php endif; ?>
						</div>
		
					</div>
		
		
					<header class="h-img">
						<div class="img i-left">
							<img alt="left image" src="<?php echo get_template_directory_uri() ?>/assets/images/square-1.jpg">
						</div>
						<div class="img i-right">
							<img alt="right image" src="<?php echo get_template_directory_uri() ?>/assets/images/square-2.jpg">
						</div>
					</header>
				</div>
			</section>
		
			<!-- Arrows scroll down/up -->
			<footer class="s-footer p-scrolldown">
				<a class="down btn" href="<?php echo site_url(); ?>/about-us">
					<span class="icon">
		                <span class="ion-ios-play ic"></span>
					</span>
					<span class="txt">Discover</span>
				</a>
			</footer>
		</div>
		<!-- End of home section -->
		
		<!-- Begin of about us section -->
		<div class="section section-about section-cent fp-auto-height-responsive section-color" data-bgcolor="rgba(59, 44, 44, 0.95)"
		 data-section="about-us">
		
			<section class="content clearfix">
				<h2 class="page-title">About</h2>
		
				<!-- Begin of centered elements -->
				<div class="c-center anim">
					<div class="wrapper">
						<!-- Header : title -->
						<header class="c-header">
		
							<h2 class="title">About</h2>
							<i class="icon lnr lnr-mustache"></i>
							<p>We are special, and here is why</p>
						</header>
                        <ul class="feature-text-list row small-up-1 medium-up-2 large-up-2">
							<!-- item -->
							<li class="column anim">
								<div class="item-desc">
									<h3 class="title">Events</h3>
									<div class="desc">
										<p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit. Maecenas a sem ultrices neque vehicula fermentum
											a sit amet nulla.</p>
									</div>
								</div>
							</li>
							<!-- item -->
							<li class="column anim">
								<div class="item-desc">
									<h3 class="title">Food Delivery</h3>
									<div class="desc">
										<p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit. Maecenas a sem ultrices neque vehicula fermentum
											a sit amet nulla.</p>
									</div>
								</div>
							</li>
							<!-- item -->
							<li class="column anim">
								<div class="item-desc">
									<h3 class="title">Dinner</h3>
									<div class="desc">
										<p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit. Maecenas a sem ultrices neque vehicula fermentum
											a sit amet nulla.</p>
									</div>
								</div>
							</li>
							<!-- item -->
							<li class="column anim">
								<div class="item-desc">
									<h3 class="title">Restaurant</h3>
									<div class="desc">
										<p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit. Maecenas a sem ultrices neque vehicula fermentum
											a sit amet nulla.</p>
									</div>
								</div>
							</li>
						</ul>
						
					</div>
				</div>
				<!-- end of centered elements -->
		
		
			</section>
		
			<!-- Arrows scroll down/up -->
			<footer class="s-footer p-scrolldown">
				<a class="down btn" href="<?php echo site_url(); ?>/#Chef">
					<span class="left">Our</span>
					<span class="icon"></span>
					<span class="right">Menu</span>
				</a>
			</footer>
		</div>
		<!-- End of about us section -->
		
		<!-- Begin of Menu section -->
		<div class="section section-menu section-cent section-color" data-bgcolor="rgba(0, 0, 0, 0.8)" data-section="menu">
		
			<section class="content clearfix">
				<h2 class="page-title">Menu</h2>
		
				<!-- centered  elements -->
				<div class="c-center anim">
					<div class="wrapper">
						<!-- Header : title -->
						<header class="c-header">
		
							<h2 class="title">Menu</h2>
							<i class="icon lnr lnr-dinner"></i>
							<!--<p>Nos spécialités</p>-->
						</header>
		
						<!-- Begin of works/services/products -->
						<div class="slider-items slider-wrapper">
							<!-- pagination -->
							<div class="items-pagination"></div>
							<div class="items-button items-button-prev">
								<a class="normal-btn inv-btn">
									<span class="txt">Previous</span>
									<span class="icon"></span>
								</a>
							</div>
							<div class="items-button items-button-next">
								<a class="normal-btn">
									<span class="icon"></span>
									<span class="txt">Next</span>
								</a>
							</div>
							<!-- slider -->
							<div class="items-slide">
								<ul class="item-list swiper-wrapper">
								    <?php

                                     $args = array(
										'post_type' => 'menu',
										'post_status' => 'publish',
                                        //'numberposts' => 6,
										'order' => 'DESC',
									);
									$arr_posts = new WP_Query( $args );	
									if ( $arr_posts->have_posts() ) { 
										while ( $arr_posts->have_posts() ) { 
										 $arr_posts->the_post();
										$content = get_the_content( 'Read more' );
										$image = get_the_post_thumbnail_url($arr_posts->ID);
										
										?>
									<!-- item -->
									<li class="item swiper-slide">
										<div class="item-box">
											<a href="<?php echo get_permalink($arr_posts->ID); ?>">
												<div class="item-img">
													<div class="img bg-img" data-image-src="<?php echo $image; ?>"></div>
												</div>
												<div class="item-desc">
													<h3 class="title"><?php echo get_the_title(); ?></h3>
													<h4><?php
						                                   $term_name=wp_get_post_terms($post->ID,'menu_categories',  array("fields" => "names"));
														    echo $term_name[0]; 
													 ?></h4>
													<div class="desc">
														<p><?php echo wp_trim_words($content,15); ?></p>
													</div>
													<!--<h3 class="price">$50</h3>-->
													<div class="btns">
														<div class="normal-btn" >
															<span class="icon"></span>
															<span class="txt">See Full Menu</span>
														</div>
													</div>
												</div>
											</a>
										</div>
									</li>
										<?php }
										} ?>
									<!-- item -->
								</ul>
							</div>
						</div>
						<!-- End of works/services/products -->
					</div>
				</div>
				<!-- end of centered elements -->
		
			</section>
		
			<!-- Arrows scroll down/up -->
			<footer class="s-footer p-scrolldown">
				<a class="down btn">
					<span class="left">Our &amp; </span>
					<span class="icon"></span>
					<span class="right">Menus</span>
				</a>
			</footer>
		</div>
		<!-- End of Menu section -->
        <!-- Begin of Chef section -->
		<div class="section section-services section-cent fh-auto fp-auto-height-responsive section-color" data-bgcolor="rgba(44, 59, 46, 0.95)"
		 data-section="Chef">
		
			<section class="content clearfix">
				<h2 class="page-title">Chef</h2>
		
				<!-- Begin of centered elements -->
				<div class="c-center anim">
					<div class="wrapper">
						<!-- Header : title -->
						<header class="c-header">
		
							<h2 class="title">Our Chef</h2>
							<i class="icon lnr lnr-gift"></i>
							<p>Catering and plenty of additional services. Our magic power can do more things</p>
						</header>
		
						<!-- Begin of features/services/offers -->
                        <ul class="feature-list">
					         <?php

                                     $args = array(
										'post_type' => 'chef',
										'post_status' => 'publish',
                                        'numberposts' => 4,
										'order' => 'DESC',
									);
									$arr_posts = new WP_Query( $args );	
									if ( $arr_posts->have_posts() ) { 
										while ( $arr_posts->have_posts() ) { 
										 $arr_posts->the_post();
			
										$content = get_the_content( 'Read more' );

										$image = get_the_post_thumbnail_url($arr_posts->ID);
										 //$image = get_field('product_image',$arr_posts->ID);
										
							?>
							<!-- item -->
							<li class="item">
								<div class="item-box">
									<figure class="item-img">
										<a>
											<div class="img bg-img" data-image-src="<?php echo $image ?>"></div>
											<!--<img src="img/items/img-sample1.jpg" alt="Featured Item">-->
										</a>
									</figure>
									<div class="item-info">
										<h2><?php echo get_the_title(); ?></h2>
										<p><?php echo wp_trim_words($content,15); ?></p>
									</div>
								</div>
							</li>
									<?php }
									} ?>	
						</ul>
						
						<!-- End of features/services/offers -->
					</div>
				</div>
		
			</section>
		
			<!-- Arrows scroll down/up -->
			<footer class="s-footer p-scrolldown">
				<a class="down btn">
					<span class="left">News &amp;</span>
					<span class="icon"></span>
					<span class="right">Events</span>
				</a>
			</footer>
		</div>
		<!-- End of Chef section -->
		<!-- Begin of News/Events/Posts section -->
		<div class="section section-posts section-cent fh-auto section-white bg-color" data-bgcolor="#efefef" data-section="News &amp; Events">
		
			<section class="content large clearfix">
				<h2 class="page-title">News &amp; Events</h2>
		
		
				<!-- centered  elements -->
				<div class="c-center anim">
					<div class="wrapper">
						<!-- Header : title -->
						<header class="c-header">
							<h2 class="title">News &amp; Events</h2>
							<i class="icon lnr lnr-calendar-full"></i>
							<p>Tous nos évènements et articles récents. Lorem ipsum magicum dolor sit amet.</p>
						</header>
		
						<!-- begin of gallery container -->
						<div class="gallery-items">
							<!-- begin of grid gallery -->
							<ul class="c-itemlist row small-up-1 medium-up-2">
								  <?php

                                     $args = array(
										'post_type' => 'news',
										'post_status' => 'publish',
                                        'numberposts' => 2,
										'order' => 'DESC',
									);
									$arr_posts = new WP_Query( $args );	
									if ( $arr_posts->have_posts() ) { 
										while ( $arr_posts->have_posts() ) { 
										 $arr_posts->the_post();
			
										$content = get_the_content( 'Read more' );

										$image = get_the_post_thumbnail_url($arr_posts->ID);
										 //$image = get_field('product_image',$arr_posts->ID);
										
										?>
											  <!-- item -->
												<li class="column anim">
													<a href="<?php echo get_permalink($arr_posts->ID); ?>">
														<div class="item-img">
															<div class="img bg-img" data-image-src="<?php echo $image ?>"></div>
														</div>
														<div class="item-desc">
															<h3 class="title"><?php echo get_the_title(); ?></h3>
															<h4>News / Local</h4>
															<div class="desc">
																<p><?php echo wp_trim_words($content,15); ?></p>
															</div>
															<h3 class="btn">Read more</h3>
														</div>
													</a>
												</li>
												<!-- item -->
									<?php	}
									}
                                     										 
									 ?>	
							</ul>
						</div>
						<!-- end of gallery container -->
					</div>
				</div>
				<!-- End of centered  elements -->
		
			</section>
		</div>
		<!-- End of News/Events/Posts Menu section -->
		<!-- Begin of location/contact/item/product detail section -->
		<div class="section section-location section-cent fh-auto section-white" data-section="location">
		
			<section class="content large clearfix">
				<h2 class="page-title">Location</h2>
		
				<!-- Begin of Content item -->
				<div class="content-item">
					<!-- right elements -->
					<div class="c-right anim">
						<!-- title and texts wrapper-->
						<div class="wrapper">
							<!--div class="featured-img bg-img" data-image-src="<?php echo get_template_directory_uri() ?>/assets/images/items/img-sample2.jpg"></div-->
							<div class="featured-map">
								<!-- add your iframe code from google maps here -->
								<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d1312.235925407806!2d2.3198181373565863!3d48.86828085543139!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2smg!4v1493927148152" width="600" height="450" allowfullscreen></iframe>
							</div>
						</div>
					</div>
					<!-- end of right elements -->
		
					<!-- left elements -->
					<div class="c-left no-clip anim">
						<div class="wrapper">
							<!-- Header : title -->
							<header class="c-header">
								<h2 class="title">AnnaMaya</h2>
								<h3>Open : Mon - Sun: 24 hrs</h3>
								<p>Nos spécialités les plus rafinées. Lorem ipsum magicum dolor sit amet.</p>
							</header>
		
							<div class="c-desc">
								<ul class="iconned-list">
									<li class="item">
										<div class="icon">
											<i class="icon lnr lnr-map"></i>
										</div>
										<div class="desc">
											<h3 class="title">Address</h3>
											<p>Asset No.1, Aerocity,  
												<br>New Delhi – 110037,India
												<br>+91 11 49031351
											</p>
										</div>
									</li>
		
									<li class="item">
										<div class="icon">
											<i class="icon lnr lnr-dinner"></i>
										</div>
										<div class="desc">
											<p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit.</p>
										</div>
									</li>
		
									<li class="item">
										<div class="icon">
											<i class="icon lnr lnr-coffee-cup"></i>
										</div>
										<div class="desc">
											<p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit.</p>
										</div>
									</li>
								</ul>
		
								<div class="item-detail">
									<a class="normal-btn" href="<?php echo site_url(); ?>/contact">
										<span class="icon"></span>
										<span class="txt">Contact us</span>
									</a>
								</div>
							</div>
		
						</div>
					</div>
					<!-- end of left elements -->
				</div>
				<!-- End of Content item -->
		
			</section>
		</div>
		<!-- End of location/contact/item/product section -->

<?php //get_template_part( 'template-parts/footer-menus-widgets' ); ?>

<?php get_footer(); ?>
