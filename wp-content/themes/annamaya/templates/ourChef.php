<?php
/**
 * Template Name: Our Chef Template
 *
 */

get_header();
?>
<!-- BEGIN OF page cover -->
	<div class="page-cover hh-cover">

		<!-- Transluscent mask as filter -->
		<div class="cover-bg-mask pos-abs full-size bg-color" data-bgcolor="#333"></div>

	</div>
	<!--END OF page cover -->


	<!-- BEGIN OF page main content -->
	<main class="page-main hh-main page-home fullpg" id="mainpage">

		<!-- Begin of header cover section -->
		<div class="section section-header section-cent"  data-section="page-top-cover">
			<div class="header-cover hh-cover">
				<!-- Cover Background -->
				<?php if( get_field('banner_image') ): ?>
				<div class="cover-bg pos-abs full-size bg-img bg-blur-0" data-image-src="<?php the_field('banner_image'); ?>"></div>
				<?php endif; ?>
		
				<!-- Linear gradient mask as filter -->
				<div class="cover-bg-mask pos-abs full-size bg-gradient" data-gradient-1="#000000" data-gradient-2="rgba(0, 0, 0, 0.2)" data-opacity="0.8"></div>
				
				<!-- Transluscent mask as filter -->
				<!-- <div class="cover-bg-mask pos-abs full-size bg-color" data-bgcolor="rgba(0, 0, 0, 0.7)"></div> -->
					
				<div id="main-page-bg" class="main-page-bg cover-bg-mask pos-abs full-size"></div>
			</div>
			
			<section class="header-text anim content large clearfix">
				<!-- centered  elements -->
				<div class="c-center">
					<div class="wrapper two-columns">
						<div class="row">
		
							<div class="columns small-12 medium-12">
								<!-- Header : title -->
								<header class="c-header small-text-center">
								<?php if( get_field('banner_title') ): ?>
									<h2 class="title"><?php the_field('banner_title'); ?></h2>
									<?php endif; ?>
									<div class="separator"></div>
									<div class="desc">
										<p></p>
									</div>
								</header>
							</div>
						</div>
		
		
					</div>
				</div>
				<!-- end of centered elements -->
			</section>
		
		</div>
		<!-- End of header cover section -->
		
		<!-- Begin of features section -->
		<div class="section section-features section-cent fp-auto-height-responsive fh-auto section-white" data-section="features">
		
			<section class="content clearfix">
                <h2 class="page-title">Our Chef</h2>
				<!-- Begin of centered elements -->
				<div class="c-center anim">
					<div class="wrapper">
						<!-- Header : title -->
						<header class="c-header">
							<i class="icon lnr lnr-flag"></i>
							<p>We are special, and here is why</p>
						</header>
		
						<ul class="feature-list">
						      <?php

                                     $args = array(
										'post_type' => 'chef',
										'post_status' => 'publish',
                                        'numberposts' => 4,
										'order' => 'DESC',
									);
									$arr_posts = new WP_Query( $args );	
									if ( $arr_posts->have_posts() ) { 
										while ( $arr_posts->have_posts() ) { 
										 $arr_posts->the_post();
			
										$content = get_the_content( 'Read more' );

										$image = get_the_post_thumbnail_url($arr_posts->ID);
										 $facebook = get_field('facebook', $arr_posts->ID);
										  $twitter = get_field('twitter', $arr_posts->ID);
										   $google = get_field('google', $arr_posts->ID);
										
							?>
							<!-- item -->
							<!-- item -->
							<li class="item">
								<div class="item-box">
									<figure class="item-img">
										<div>
											<div class="img bg-img" data-image-src="<?php echo $image ?>"></div>
										</div>
										<div class="caption-bottom">
											<a class="share-btn ion-social-facebook"  href="<?php if(!empty($facebook)) echo $facebook; ?>" target="_blank"></a>
											<a class="share-btn ion-social-twitter"  href="<?php if(!empty($twitter)) echo $twitter; ?>" target="_blank"></a>
											<a class="share-btn ion-social-google"  href="<?php if(!empty($google)) echo $google; ?>" target="_blank"></a>
										</div>
									</figure>
									<div class="item-info">
										<h2><?php echo get_the_title(); ?></h2>
										<p><?php echo wp_trim_words($content,15); ?></p>
									</div>
								</div>
							</li>
						  <?php } 
							} ?>	
						</ul>
					</div>
				</div>
				<!-- end of centered elements -->
		
		
			</section>
		
			<!-- Arrows scroll down/up -->
			<!--footer class="s-footer p-scrolldown">
				<a class="down btn" href="#services">
					<span class="left">Activities</span>
					<span class="icon"></span>
					<span class="right">Services</span>
				</a>
			</footer-->
		</div>
		<!-- End of features section -->		
		<!-- End of article/description section -->
	<?php get_footer(); ?>
