<?php
/**
 * Template Name: Create Order Page Template
 *
 */

get_header();
?>
<!-- BEGIN OF page cover -->
	<div class="page-cover hh-cover">

		<!-- Transluscent mask as filter -->
		<div class="cover-bg-mask pos-abs full-size bg-color" data-bgcolor="#333"></div>

	</div>
	<!--END OF page cover -->


	<!-- BEGIN OF page main content -->
	<main class="page-main hh-main page-home fullpg" id="mainpage">

		<!-- Begin of header cover section -->
		<div class="section section-header section-cent"  data-section="page-top-cover">
			<div class="header-cover hh-cover">
				<!-- Cover Background -->
				<?php if( get_field('banner_image') ): ?>
				<div class="cover-bg pos-abs full-size bg-img bg-blur-0" data-image-src="<?php the_field('banner_image'); ?>"></div>
				<?php endif; ?>
		
				<!-- Linear gradient mask as filter -->
				<div class="cover-bg-mask pos-abs full-size bg-gradient" data-gradient-1="#000000" data-gradient-2="rgba(0, 0, 0, 0.2)" data-opacity="0.8"></div>
				
				<!-- Transluscent mask as filter -->
				<!-- <div class="cover-bg-mask pos-abs full-size bg-color" data-bgcolor="rgba(0, 0, 0, 0.7)"></div> -->
					
				<div id="main-page-bg" class="main-page-bg cover-bg-mask pos-abs full-size"></div>
			</div>
		
			
			<section class="header-text anim content large clearfix">
				<!-- centered  elements -->
				<div class="c-center">
					<div class="wrapper two-columns">
						<div class="row">
		
							<div class="columns small-12 medium-12">
								<!-- Header : title -->
								<header class="c-header small-text-center">
								<?php if( get_field('banner_title') ): ?>
									<h2 class="title"><?php the_field('banner_title'); ?></h2>
									<?php endif; ?>
									<div class="separator"></div>
									<div class="desc">
										<p></p>
									</div>
								</header>
							</div>
		
						</div>
		
		
					</div>
				</div>
				<!-- end of centered elements -->
			</section>
		
		</div>
		<!-- End of header cover section -->
		
		<!-- Begin of Products/All Menu section -->
		<div class="section section-products section-cent fh-auto bg-color" data-bgcolor="rgb(239, 239, 239)"  data-section="create-Order">
		
			<section class="content large clearfix">
				<h2 class="page-title">Create Order</h2>
				 <?php
				    session_start();
                    $total_price = 0;
					$total_item = 0;

					$output = '
					<div class="table-responsive" id="order_table">
					 <table class="table table-bordered table-striped">
					  <tr>  
								<th width="40%">Product Name</th>  
								<th width="10%">Quantity</th>  
								<th width="20%">Price</th>  
								<th width="15%">Total</th>  
								 
							</tr>
					';
					if(!empty($_SESSION["shopping_cart"]))
					{
					 foreach($_SESSION["shopping_cart"] as $keys => $values)
					 {
					  $output .= '
					  <tr>
					   <td>'.$values["product_name"].'</td>
					   <td>'.$values["product_quantity"].'</td>
					   <td align="right">₹ '.$values["product_price"].'</td>
					   <td align="right">₹ '.number_format($values["product_quantity"] * $values["product_price"], 2).'</td>
					   
					  </tr>
					  ';
					  $total_price = $total_price + ($values["product_quantity"] * $values["product_price"]);
					  $total_item = $total_item + 1;
					 }
					 $output .= '
					 <tr>  
							<td colspan="3" align="right">Total</td>  
							<td align="right">₹ '.number_format($total_price, 2).'</td>  
							
						</tr>
					 ';
					}
					else
					{
					 $output .= '
						<tr>
						 <td colspan="5" align="center">
						  Your Cart is Empty!
						 </td>
						</tr>
						';
					}
					$output .= '</table></div>';
					$data = array(
					 'cart_details'  => $output,
					 'total_price'  => '₹' . number_format($total_price, 2),
					 'total_item'  => $total_item
					); 
				 ?>
				 	<!-- centered  elements -->
					
				<div style="color:#333333">
					<div class="">
							 <?php  echo $data['cart_details']; ?>
					</div>
				</div>
				<!-- End of centered  elements -->
				<div class="container-contact100">
					<div class="wrap-contact100">
						<form class="contact100-form validate-form" id="orderForm" action="<?php echo admin_url( 'admin-post.php' ); ?>" method="post">
							<span class="contact100-form-title">
								 Create Order
							</span>

							<div class="wrap-input100 rs1-wrap-input100 validate-input" data-validate="Name is required">
								<span class="label-input100">Name</span>
								<input class="input100" type="text" name="name" placeholder="Enter your name">
								<span class="focus-input100"></span>
							</div>

							<div class="wrap-input100 rs1-wrap-input100 validate-input" data-validate = "Contact Number is required">
								<span class="label-input100">Contact Number</span>
								<input class="input100 numbersOnly" type="text" name="contactnumber" placeholder="Enter your contact number">
								<span class="focus-input100"></span>
							</div>
							
							<div class="wrap-input100  validate-input" data-validate = "Valid email is required: ex@abc.xyz">
								<span class="label-input100">Email Address</span>
								<input class="input100" type="text" name="email" placeholder="Enter your email addess">
								<span class="focus-input100"></span>
							</div>

							<div class="wrap-input100 validate-input" data-validate = "Message is required">
								<span class="label-input100">Comments/Specific Requirement</span>
								<textarea class="input100" name="message" placeholder="Your message here..."></textarea>
								<span class="focus-input100"></span>
							</div>
                            
							<div class="container-contact100-form-btn">
							    <input type="hidden" name="action" value="create_order" />
								<button class="contact100-form-btn">
									<span>
										Submit
										<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
									</span>
								</button>
							</div>
						</form>

						<span class="contact100-more">
							For any question contact our 24/7 call center: <span class="contact100-more-highlight">+001 345 6889</span>
						</span>
					</div>
				</div>

                
			</section>
		</div>
		<!-- End of Products/All Menu section -->
	<?php get_footer(); ?>
