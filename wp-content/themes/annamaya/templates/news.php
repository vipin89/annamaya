<?php
/**
 * Template Name: News Template
 *
 */

get_header();
?>
<!-- BEGIN OF page cover -->
	<div class="page-cover hh-cover">

		<!-- Transluscent mask as filter -->
		<div class="cover-bg-mask pos-abs full-size bg-color" data-bgcolor="#333"></div>

	</div>
	<!--END OF page cover -->


	<!-- BEGIN OF page main content -->
	<main class="page-main hh-main page-home fullpg" id="mainpage">

		<!-- Begin of header cover section -->
		<div class="section section-header section-cent"  data-section="page-top-cover">
			<div class="header-cover hh-cover">
				<!-- Cover Background -->
				<div class="cover-bg pos-abs full-size bg-img bg-blur-0" data-image-src="<?php echo get_template_directory_uri() ?>/assets/images/bg-default5.jpg"></div>
		
				<!-- Linear gradient mask as filter -->
				<div class="cover-bg-mask pos-abs full-size bg-gradient" data-gradient-1="#000000" data-gradient-2="rgba(0, 0, 0, 0.2)" data-opacity="0.8"></div>
				
				<!-- Transluscent mask as filter -->
				<!-- <div class="cover-bg-mask pos-abs full-size bg-color" data-bgcolor="rgba(0, 0, 0, 0.7)"></div> -->
					
				<div id="main-page-bg" class="main-page-bg cover-bg-mask pos-abs full-size"></div>
			</div>
		
			
			<section class="header-text anim content large clearfix">
				<!-- centered  elements -->
				<div class="c-center">
					<div class="wrapper two-columns">
						<div class="row">
		
							<div class="columns small-12 medium-12">
								<!-- Header : title -->
								<header class="c-header small-text-center">
									<h2 class="title">News &amp; Events</h2>
									<div class="separator"></div>
									<div class="desc">
										<p></p>
									</div>
								</header>
							</div>
		
						</div>
		
		
					</div>
				</div>
				<!-- end of centered elements -->
			</section>
		
		</div>
		<!-- End of header cover section -->
		
		<!-- Begin of News/Events/Posts section -->
		<div class="section section-posts section-cent fh-auto section-white bg-color" data-bgcolor="#ffffff" data-section="Events">
		
			<section class="content large clearfix">
				<h2 class="page-title">Events</h2>
		
		
				<!-- centered  elements -->
				<div class="c-center anim">
					<div class="wrapper">
						<!-- Header : title -->
						<header class="c-header">
							<h2 class="title">Events</h2>
							<i class="icon lnr lnr-calendar-full"></i>
							<p>Tous nos évènements et articles récents. Lorem ipsum magicum dolor sit amet.</p>
						</header>
		
						<!-- begin of gallery container -->
						<div class="gallery-items">
							<!-- begin of grid gallery -->
							<ul class="c-itemlist row small-up-1 medium-up-2">
								 
								<!-- item -->
								<li class="column anim">
									<a href="news-item.html">
										<div class="item-img">
											<div class="img bg-img" data-image-src="<?php echo get_template_directory_uri() ?>/assets/images/items/img-sample3.jpg"></div>
										</div>
										<div class="item-desc">
											<h3 class="title">Nouveau Chef</h3>
											<h4>News / Local</h4>
											<div class="desc">
												<p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit. Maecenas a sem ultrices neque vehicula fermentum
													a sit amet nulla.</p>
											</div>
											<h3 class="btn">Read more</h3>
										</div>
									</a>
								</li>
								<!-- item -->
								<li class="column anim">
									<a href="news-item.html">
										<div class="item-img">
											<div class="img bg-img" data-image-src="<?php echo get_template_directory_uri() ?>/assets/images/items/img-sample4.jpg"></div>
										</div>
										<div class="item-desc">
											<h3 class="title">Moka Coffee</h3>
											<h4>Recipe / Speciality</h4>
											<div class="desc">
												<p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit. Maecenas a sem ultrices neque vehicula fermentum
													a sit amet nulla.</p>
											</div>
											<h3 class="btn">Read more</h3>
										</div>
									</a>
								</li>
							</ul>
						</div>
						<!-- end of gallery container -->
					</div>
				</div>
				<!-- End of centered  elements -->
		
			</section>
		</div>
		<!-- End of News/Events/Posts Menu section -->

		<!-- Begin of News/Events/Posts section -->
		<div class="section section-posts section-cent fh-auto section-white bg-color" data-bgcolor="#efefef" data-section="Recipes">
		
			<section class="content large clearfix">
				<h2 class="page-title">Recipes</h2>
		
		
				<!-- centered  elements -->
				<div class="c-center anim">
					<div class="wrapper">
						<!-- Header : title -->
						<header class="c-header">
							<h2 class="title">Recipes</h2>
							<i class="icon lnr lnr-license"></i>
							<p>Tous nos évènements et articles récents. Lorem ipsum magicum dolor sit amet.</p>
						</header>
		
						<!-- begin of gallery container -->
						<div class="gallery-items">
							<!-- begin of grid gallery -->
							<ul class="c-itemlist row small-up-1 medium-up-2">
								 
								<!-- item -->
								<li class="column anim">
									<a href="news-item.html">
										<div class="item-img">
											<div class="img bg-img" data-image-src="<?php echo get_template_directory_uri() ?>/assets/images/items/img-sample3.jpg"></div>
										</div>
										<div class="item-desc">
											<h3 class="title">Nouveau Chef</h3>
											<h4>News / Local</h4>
											<div class="desc">
												<p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit. Maecenas a sem ultrices neque vehicula fermentum
													a sit amet nulla.</p>
											</div>
											<h3 class="btn">Read more</h3>
										</div>
									</a>
								</li>
								<!-- item -->
								<li class="column anim">
									<a href="news-item.html">
										<div class="item-img">
											<div class="img bg-img" data-image-src="<?php echo get_template_directory_uri() ?>/assets/images/items/img-sample4.jpg"></div>
										</div>
										<div class="item-desc">
											<h3 class="title">Moka Coffee</h3>
											<h4>Recipe / Speciality</h4>
											<div class="desc">
												<p>Lorem ipsum magicum dolor sit amet, consectetur adipiscing elit. Maecenas a sem ultrices neque vehicula fermentum
													a sit amet nulla.</p>
											</div>
											<h3 class="btn">Read more</h3>
										</div>
									</a>
								</li>
							</ul>
						</div>
						<!-- end of gallery container -->
					</div>
				</div>
				<!-- End of centered  elements -->
		
			</section>
		</div>
		<!-- End of News/Events/Posts Menu section -->
		
	<?php get_footer(); ?>
