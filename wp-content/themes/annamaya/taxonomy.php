<?php
/**
* A Simple taxonomy Template
*/
 
get_header(); 
$category = get_queried_object();
$image = get_field('image', 'category_'.$category->term_id);	
$price = get_field('price', 'category_'.$category->term_id);
 ?> 
 

<!-- BEGIN OF page cover -->
	<div class="page-cover hh-cover">

		<!-- Transluscent mask as filter -->
		<div class="cover-bg-mask pos-abs full-size bg-color" data-bgcolor="#333"></div>

		<div id="main-page-bg" class="main-page-bg cover-bg-mask pos-abs full-size"></div>
	</div>
	<!--END OF page cover -->


	<!-- BEGIN OF page main content -->
	<main class="page-main hh-main page-home fullpg" id="mainpage">

		<!-- Begin of item detail section -->
		<div class="section section-item-detail section-menu section-cent  section-color" data-color="rgba(34, 28, 30, 0.95)" data-section="menu-item">
		
			<section class="content large clearfix">
				<!--				<h2 class="page-title">Cordon Sauté</h2>-->
		
				<!-- centered  elements -->
				<div class="c-center  anim">
					<div class="wrapper item-detail two-columns">
						<div class="row">
		
							<div class="columns small-12 medium-6">
								<!-- Header : title -->
								<header class="c-header small-text-center medium-text-left">
									<h2 class="title"><?php echo single_cat_title( '', false ); ?></h2>
									<!--<p>LeChef secret sauce</p>-->
								</header>
								<!-- description -->
								<div class="item-desc small-text-center medium-text-left">
									<h3 class="title">Du sauté à LeChef</h3>
									<div class="desc">
										<?php
									// Display optional category description
									 if ( category_description() ) : ?>
									<p><?php echo category_description(); ?></p>
									<?php endif; ?>
									</div>
									<h3 class="price"><?php if(!empty($price)){ echo $price; } ?></h3>
								</div>
							</div>
		
							<div class="columns small-12 medium-6">
								<!-- illustration -->
								<div class="item-img">
									<!-- use image or div tag -->
									<!-- <img src="img/items/img-sample2.jpg" alt="menu image">-->
									<?php if(!empty($image) ): ?>
										<div class="img bg-img" data-image-src="<?php  echo $image; ?>"></div>
										<?php endif; ?>
								</div>
							</div>
		
						</div>
		
		
					</div>
				</div>
				<!-- end of centered elements -->
			</section>
		
		</div>
		<!-- End of item detail section -->
		 <?php  
			     $args = array(
						'type'                     => 'menu',
						'orderby'                  => 'name',
						'order'                    => 'ASC',
						'hide_empty'               => FALSE,
						'hierarchical'             => 1,
						'taxonomy'                 => 'menu_item_categories',
						); 
						$child_categories = get_categories($args );
		if(!empty($child_categories))	{ ?>
		<!-- Begin of Products/All Menu section -->
		<div class="section section-products section-cent fh-auto bg-color" data-bgcolor="rgba(22, 16, 45, 0.95)"  data-section="menu-list">
			<section class="content large clearfix">
				<h2 class="page-title">Soup</h2>
		
		
				<!-- centered  elements -->
				<div class="c-center anim">
					<div class="wrapper">
						<!-- Header : title -->
						<header class="c-header">
							<h2 class="title">Menu</h2>
							<i class="icon lnr lnr-dinner"></i>
						</header>
		
						<!-- begin of gallery container -->
						<div class="gallery-items">
							<!-- begin of grid gallery -->
							<ul class="c-itemlist row small-up-1 medium-up-2">
							<?php foreach($child_categories as $categories){  $image = get_field('image', $categories); 
                              $price = get_field('price', $categories);

							?>
								<!-- item -->
								<li class="column anim">
									<a href="<?php echo get_category_link($categories); ?>">
										<div class="item-img">
											<div class="img bg-img" data-image-src="<?php echo $image; ?>"></div>
										</div>
										<div class="item-desc">
											<h3 class="title"><?php echo $categories->name ?></h3>
											<div class="desc">
												<p><?php echo wp_trim_words($categories->description,15); ?></p>
											</div>
											<h3 class="price"><?php if(!empty($price)) { echo $price; }  ?></h3>
										</div>
									</a>
								</li>
							<?php } ?>	
							</ul>
						</div>
						<!-- end of gallery container -->
					</div>
				</div>
				<!-- End of centered  elements -->
		
			</section>
		</div>
		<!-- End of Products/All Menu section -->		
		<?php } ?>
<?php get_footer(); ?>