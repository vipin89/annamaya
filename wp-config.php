<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'annamaya' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '!9y1ebe{WGFg!t4H|ctZ{C`W7@p>g!l%J<9MD|)x<CY-0&CfnOeM7pbc48ea?T;*' );
define( 'SECURE_AUTH_KEY',  '$i_Mqer0&cb0Q*^Oaa9$NDTR=((=>#-u]=S8gisK]NO!TYH,-jjK2<=|@?FP07b*' );
define( 'LOGGED_IN_KEY',    'p*N<cgV2<=T:N,CHJ.!-z]73m ,q1&|SA-8qD 1*vKi$Vb@U|&O|RV8Z8y$,m(Ck' );
define( 'NONCE_KEY',        'WyMHvo|13Vid,Qh77Yo:Y,Ni>N]`UtP2?McI[xB_K(cKBo:A.sDPqysY[H26OnoE' );
define( 'AUTH_SALT',        '6T38Wfo[,sp55)_:ESOpHK/Vz$F.n%&9QDzV_&~we36GOFtPTp;(GNDz@.*%b>Bi' );
define( 'SECURE_AUTH_SALT', '&m-W6)*J-;|JPO[x?Dhlmz33Rz&nKsJwPocK),[>$U[c|4QBj#{7DUgl:%.x;RG ' );
define( 'LOGGED_IN_SALT',   'dkjq)eP7skM78M.)`b/q1Z6jsB/b) (y|1bnd)a2+n1X{-v;;81^X@.mrX&2 6#7' );
define( 'NONCE_SALT',       'nXKN8 Yf=L0ZJnNOpo?K=g]-GG2!zC}-~U^WQly>/8&wA%^?mK-a~dN3~]%jvw71' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
